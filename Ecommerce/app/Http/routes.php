


<?php

Route ::post('login','eusersController@login');
Route ::post('register','eusersController@siginUp');
Route ::get('allUsers','eusersController@allUsers');
Route ::get('myProfile/{id}','eusersController@myProfile');

Route ::post('addProduct','productsController@add');
Route ::get('getAllProducts','productsController@getAll');
Route ::get('getMyAdds/{id}','productsController@userAllProdcts');
Route ::post('updateProduct','productsController@update');
Route ::delete('deleteProduct/{id}','productsController@delete');

Route ::post('saveBid','biddsController@save');
Route ::post('acceptedBid','biddsController@accept');
Route ::get('allBids','biddsController@allBids');

?>
