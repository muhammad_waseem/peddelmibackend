<?php 

namespace App\Http\Controllers;

use App\eusers;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class eusersController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        // Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\eusers());
			$this->filter->add('first_name', 'Name', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('first_name', 'First Name');
			$this->grid->add('last_name', 'Last Name');
			$this->grid->add('email', 'Email');
			$this->grid->add('password', 'Password');

			$this->addStylesToGrid();


                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

         //Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\eusers());

			$this->edit->label('Edit Category');

			$this->edit->add('name', 'Name', 'text');
		
			$this->edit->add('code', 'Code', 'text')->rule('required');
        return $this->returnEditView();
    }

	public function siginUp(Request $request)
	{

		$firstName = $request->json()->get('first_name');
		$lastName = $request->json()->get('last_name');
		$email = $request->json()->get('email');
		$password = $request->json()->get('password');

		$query = DB::table('eusers')->where('email', $email)->where('password', '=', $password)->get();


		if ($query == null) {

			$user = new eusers();
			$user->first_name = $firstName;
			$user->last_name = $lastName;
			$user->password = $password;
			$user->email = $email;

			$insertStatus = $user->save();

			if ($insertStatus == 1) {
				$query = DB::table('eusers')->where('email', $email)->where('password', '=', $password)->get();
				$response = array('message' => "login successfully",
						'data' => $query,
						'success' => true);
				return $response;
			} else {
				$response = array('message' => "register faild",
						'success' => false);
				return $response;
			}
		} else {

			$response = array('message' => "user already exist",
					'success' => false);
			return $response;
		}
	}


	public function login(Request $request){


		$requestEmail= $request->json()->get('email');
		$requestPassword= $request->json()->get('password');
		$query = DB::table('eusers')->where('email', $requestEmail)->where('password', '=', $requestPassword)->get();
		if($query !=null ){
			$response = array('message' => "login successfully",
					'data' => $query,
					'success' => true);
			return $response;
		}
		else{
			$response = array('message' => "login faild",
					'success' => false);
			return $response;
		}


	}
	public function allUsers()
	{

		$users = eusers::all();

		$response = array('users' => $users,
				'success' => true);
		return $response;
	}

	public function myProfile($id)
	{
		$myContent = DB::table('eusers')->where('id', '=',$id )->get();

		$response = array('message' => "ok",
				'data' => $myContent,
				'success' => true);
		return $response;

	}
}
