<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;
use DB;
use App\products;
use Illuminate\Http\Request;

class productsController extends CrudController
{

    public function all($entity)
    {
        parent::all($entity);

        // Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


        $this->filter = \DataFilter::source(new \App\products);
        $this->filter->add('name', 'Name', 'text');
        $this->filter->submit('search');
        $this->filter->reset('reset');
        $this->filter->build();

        $this->grid = \DataGrid::source($this->filter);
        $this->grid->add('id', 'Id');
        $this->grid->add('name', 'Name');
        $this->grid->add('code', 'Code');
        $this->grid->add('status', 'Status');
        $this->grid->add('created_at', 'Created At');
        $this->grid->add('updated_at', 'Updated At');
        $this->addStylesToGrid();

        return $this->returnView();
    }

    public function  edit($entity)
    {

        parent::edit($entity);

        // Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields

        $this->edit = \DataEdit::source(new \App\products());

        $this->edit->label('Edit Category');

        $this->edit->add('name', 'Name', 'text');

        $this->edit->add('code', 'Code', 'text')->rule('required');

        return $this->returnEditView();
    }

    public function add(Request $request)
    {

        $userId = $request->json()->get('userId');
        $productDetail = $request->json()->get('detail');
        $productContact = $request->json()->get('contact');
        $productCondition = $request->json()->get('condition');
        $productBrand = $request->json()->get('brand');
        $productLocation = $request->json()->get('location');
        $productName = $request->json()->get('name');
        $productCode = $request->json()->get('code');
        $status = $request->json()->get('status');
        $productPrice = $request->json()->get('price');

        $product = new products();
        $product->user_id = $userId;
        $product->detail = $productDetail;
        $product->contact = $productContact;
        $product->brand = $productBrand;
        $product->location = $productLocation;
        $product->name = $productName;
        $product->code = $productCode;
        $product->price = $productPrice;
        $product->status = $status;
        $product->present_condition = $productCondition;
        $insertStatus = $product->save();

        if ($insertStatus == 1) {
            $query = DB::table('products')->where('user_id', $userId)->where('contact', $productContact)->where('code', '=', $productCode)->where('price', '=', $productPrice)->get();
            $response = array('message' => 'added successfully',
                'data' => $query,
                'success' => true);
            return $response;
        } else {
            $response = array('message' => 'faild',
                'success' => true);
            return $response;
        }
    }


    public function getAll()
    {
        $products = DB::table('products')->where('status', '=', 'active') ->orderBy('id', 'desc')->get();


        $response = array('products' => $products,
            'success' => true);
        return $response;

    }

    public function userAllProdcts($id)
    {
        $products = DB::table('products')->where('user_id', '=', $id)->get();

        $response = array('message' => "ok",
            'data' => $products,
            'success' => true);
        return $response;

    }

    public function delete($id)
    {
        $varStatus = DB::table('products')->where('id', '=', $id)->delete();

        if ($varStatus == 1) {
            $response = array('message' => "Successfully deleted",
                'success' => true);
            return $response;
        } else {

            $response = array('message' => "row not deleted",
                'success' => true);
            return $response;
        }
    }

    public function update(Request $request)
    {
        $userId = $request->json()->get('userId');
        $productDetail = $request->json()->get('detail');
        $productContact = $request->json()->get('contact');
        $productCondition = $request->json()->get('condition');
        $productBrand = $request->json()->get('brand');
        $productLocation = $request->json()->get('location');
        $productName = $request->json()->get('name');
        $productCode = $request->json()->get('code');
        $status = $request->json()->get('status');
        $productPrice = $request->json()->get('price');

        $status = DB::table('products')
            ->where('user_id', $userId)
            ->update(array('detail' => $productDetail,
                'contact' => $productContact,
                'brand' => $productBrand,
                'location' => $productLocation,
                'price' => $productPrice,
                'name' => $productName,
                'code' => $productCode,
                'status' => $status,
                'present_condition' => $productCondition
            ));

        if ($status == 1) {
            $response = array('message' => "ok",
                'success' => true);
            return $response;
        } else {
            $response = array('message' => "ok",
                'success' => false);
            return $response;
        }
    }
}
