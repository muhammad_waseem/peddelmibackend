<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;
use DB;
use App\bidds;
use Illuminate\Http\Request;

class biddsController extends CrudController
{

    public function all($entity)
    {
        parent::all($entity);

        // Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


        $this->filter = \DataFilter::source(new \App\bidds);
        $this->filter->add('id', 'Bidd iD', 'text');
        $this->filter->submit('search');
        $this->filter->reset('reset');
        $this->filter->build();

        $this->grid = \DataGrid::source($this->filter);
        $this->grid->add('id', 'Bidd Id');
        $this->grid->add('status', 'Status');
        $this->grid->add('product_id', 'Product Id');
        $this->grid->add('users_id', 'User Id');
        $this->addStylesToGrid();
        return $this->returnView();
    }

    public function  edit($entity)
    {

        parent::edit($entity);

        // Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields

        $this->edit = \DataEdit::source(new \App\bidds());

        $this->edit->label('Edit Category');

        $this->edit->add('name', 'Name', 'text');

        $this->edit->add('code', 'Code', 'text')->rule('required');
        return $this->returnEditView();
    }

    public function save(Request $request)
    {

        $productId = $request->json()->get('product_id');
        $usersId = $request->json()->get('users_id');
        $status = $request->json()->get('status');
        $bidd = new bidds();
        $bidd->product_id = $productId;
        $bidd->users_id = $usersId;
        $bidd->status = $status;
        $insertStatus = $bidd->save();

        if ($insertStatus == 1) {
            return 'bidd added';
        } else {
            return 'faild';
        }
    }

    public function accept(Request $request)
    {

        $biddsId = $request->json()->get('bid_id');
        $status = $request->json()->get('status');
        $updatedStuats = DB::table('bidds')->where('id', $biddsId)->update(array('status' => $status));
        $row = bidds::find($biddsId);
        $productId = $row->product_id;

        if ($updatedStuats == 1) {
            echo 'updated successfully';
            $this->otherBiddsItemStatus($productId);
            $this->changeProductItemStatus($productId);
        } else {
            return 'nothing updated';
        }

    }

    public function changeProductItemStatus($productId)
    {
        $updatedStuats = DB::table('products')->where('id', $productId)->update(array('status' => 'In Active'));
//       return $updatedStuats;
        if ($updatedStuats == 1) {
            return '1';
        } else {
            return '0';
        }
    }

    public function otherBiddsItemStatus($productId)
    {

        $updatedStuats = DB::table('bidds')->where('product_id', $productId)->where('status', '!=', 'accepted')->update(array('status' => 'failure'));
        if ($updatedStuats == 1) {
            return '1';
        } else {
            return '0';
        }
    }

    public function allBids()
    {

        $allBids = DB::table('bidds')->where('status', 'accepted')->get();
        return $allBids;

    }

}
